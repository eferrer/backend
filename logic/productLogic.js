const catchAsync = require('../utils/catchAsync');
const productService = require('../services/productService');
const cache = require('../storage/cache');
const logger = require('./../config/logger');

const PRODUCTS_CACHE_TTL = process.env.PRODUCT_CACHE_TTL || 120;
const PRODUCTS_CACHE_KEY = process.env.PRODUCT_CACHE_KEY || 'products';

exports.getAllProducts = async () => {
    logger.debug(`[GET_PRODUCTS] Looking for key ${PRODUCTS_CACHE_KEY} in the cache`);
    const products = await cache.getAsync(PRODUCTS_CACHE_KEY);
    if (products != null) {
        logger.debug(`Cache key ${PRODUCTS_CACHE_KEY} found`);
        return JSON.parse(products);
    } else {
        logger.debug(`Cache key ${PRODUCTS_CACHE_KEY} not found`);
        const products = await productService.getAllProducts();
        logger.debug('Products retrieved from Ripley service.');

        if (products && products.length > 0) {
            const productsStr = JSON.stringify(products);
            logger.debug(`Caching products for ${PRODUCTS_CACHE_TTL} with key ${PRODUCTS_CACHE_KEY}.`);
            cache.setex(PRODUCTS_CACHE_KEY, productsStr, PRODUCTS_CACHE_TTL);
        }
        return products;
    }
};

exports.getProduct = async (partNumber) => {
    logger.debug(`[GET_PRODUCT] Looking for key ${PRODUCTS_CACHE_KEY} in the cache`);

    const products = await cache.getAsync(PRODUCTS_CACHE_KEY);
    if (products != null) {
        logger.debug(`[GET_PRODUCT] Cache key ${PRODUCTS_CACHE_KEY} found`);
        const productsArray = JSON.parse(products).filter(product => {
            return product.partNumber === partNumber;
        });
        return productsArray.length > 0 ? productsArray[0] : {}
    } else {
        return await productService.getProduct(partNumber);
    }
};
