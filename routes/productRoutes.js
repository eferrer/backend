const express = require('express');
const authController = require('./../controllers/authController');
const productController = require('./../controllers/productController');

const router = express.Router();

router
    .route('/')
    .get(
        authController.protect,
        productController.getAllProducts
    );

router
    .route('/:partNumber')
    .get(
        authController.protect,
        productController.getProduct
    );

module.exports = router;
