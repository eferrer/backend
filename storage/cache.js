const redis = require('redis');
const {promisify} = require('util');

const client = redis.createClient(process.env.REDIS_URL);

exports.setex = async (key, value, ttl) => {
    client.set(key, value);
    client.expire(key, ttl);
};

exports.getAsync = promisify(client.get).bind(client);
