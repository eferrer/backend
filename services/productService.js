const axios = require('axios');
const AppError = require('./../utils/appError');
const logger = require('./../config/logger');

// Aunque el requerimiento dice 'indefinidamente' me parece correcto definir un límite configurable
const ERROR_RETRIES = process.env.PRODUCT_ERROR_RETRIES;
const ERROR_PERCENT = process.env.PRODUCT_ERROR_PERCENT;

const getRandomInt = max =>  {
    return Math.floor(Math.random() * Math.floor(max));
};

exports.getAllProducts = async () => {
    const products = process.env.PRODUCTS;
    const url = `${process.env.SIMPLE_API_URL}?partNumbers=${process.env.PRODUCTS}?format=json`;

    let status = 0;
    let tries = 1;
    let response = null;

    while (status !== 200 && tries <= ERROR_RETRIES) {
        logger.debug(`[GET_PRODUCTS] try ${tries} of ${ERROR_RETRIES}.`);
        try {
            // Las consultas a la API de simple.ripley.cl deben simular una tasa de error del 15% de manera
            // aleatoria (hint: utilizar Math.random)
            if (getRandomInt(100) < ERROR_PERCENT) {
                logger.debug("Forcing 15% of requests failures to ripley API");
                throw new AppError('', 500);
            }
            response = await axios.get(url);
            status = response.status;
        } catch (error) {
            logger.error(`Error querying to ripley API, error: ${JSON.stringify(error)}`);
            if (error instanceof AppError) {
                status = error.statusCode;
            } else {
                status = error.response.status;
            }
        }
        tries += 1
    }
    return response != null ? response.data : [];
};

exports.getProduct = async (partNumber) => {
    const url = `${process.env.SIMPLE_API_URL}/${partNumber}`;

    let status = 0;
    let tries = 1;
    let response = null;

    while (status !== 200 && tries <= ERROR_RETRIES) {
        logger.debug(`[GET_PRODUCTS] try ${tries} of ${ERROR_RETRIES}.`);
        try {
            response = await axios.get(url);
            status = response.status;
        } catch (error) {
            status = error.response.status;
        }
        tries += 1
    }
    return response != null ? response.data : {};
};
