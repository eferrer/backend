const catchAsync = require('../utils/catchAsync');
const productLogic = require('../logic/productLogic');

exports.getAllProducts = catchAsync(async (req, res, next) => {
    const products = await productLogic.getAllProducts();

    res.status(200).json({
        status: 'success',
        data: {
            products
        }
    });
});

exports.getProduct = catchAsync(async (req, res, next) => {
    const partNumber = req.params.partNumber;
    const product = await productLogic.getProduct(partNumber);

    res.status(200).json({
        status: 'success',
        data: {
            product
        }
    });
});
