# Desafío Ripley - Backend 

API Rest escrita con NodeJs la cual expone dos servicios, uno para devolver un catálogo de productos obtenidos desde la API de simple.ripley.cl y otro para devolver un único producto. Ambos servicios validan el atributo `idToken` enviado en el header contra los servicios de Firebase para identificar si hay una sesión activa para el usuario. El catálogo de productos obtenidos de la API de Ripley es guardado en REDIS por 2 minutos y es utilizado en posteriores consultas.

 
## Como instalar localmente
1. [Descarga](https://gitlab.com/desafio-ripley/frontend/-/archive/backend/backend-develop.zip) o clona el [repositorio](https://gitlab.com/desafio-ripley/backend):
```bash
git clone https://gitlab.com/desafio-ripley/backend.git
```
2. Entra al directorio del proyecto
```bash
cd backend
```
3. Instala las dependencias
```bash
npm install
```
4. Crear archivo `config/serviceAccountKey.json` con la clave de la cuenta de servicio de Google para validar el token de autenticación. Ejemplo de contenido:
```json
{
    "type": "service_account",
    "project_id": "desafio-ripley-20200516",
    "private_key_id": "[hash]",
    "private_key": "-----BEGIN PRIVATE KEY-----\n[CONTENT]\n-----END PRIVATE KEY-----\n",
    "client_email": "firebase-adminsdk-[text]@desafio-ripley-20200516.iam.gserviceaccount.com",
    "client_id": "[Number]",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-gubzb%40desafio-ripley-20200516.iam.gserviceaccount.com"
}
```
5. Inicia el servidor localmente.
```bash
node server.js
```

## Tecnologías
El proyecto fue creado con:

- node 10
- express 4
- firebase-admin 8
- redis 3
