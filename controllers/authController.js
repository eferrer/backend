const catchAsync = require('./../utils/catchAsync');
const AppError = require('./../utils/appError');
const logger = require('./../config/logger');
const admin = require('firebase-admin');

const firebase_private_key_b64 = Buffer.from(process.env.FIREBASE_PRIVATE_KEY_BASE64, 'base64');
const firebase_private_key = firebase_private_key_b64.toString('utf8');

admin.initializeApp({
    credential: admin.credential.cert({
        "project_id": process.env.FIREBASE_PROJECT_ID,
        "private_key": firebase_private_key,
        "client_email": process.env.FIREBASE_CLIENT_EMAIL,
    }),
    databaseURL: process.env.FIREBASE_DATABASE_URL
});

exports.protect = catchAsync(async (req, res, next) => {
    //logger.debug(req.headers);
    const token = req.headers.idtoken;

    if (!token) {
        return next(
            new AppError('You are not logged in! Please log in to get access.', 401)
        );
    }

    try {
        const decodeToken = await admin.auth().verifyIdToken(token);
        logger.debug(`User with email ${decodeToken.email} is logged in!`);
    } catch (error) {
        return next(
            new AppError('The user belonging to this token does no longer exist.', 401)
        );
    }
    next();
});
