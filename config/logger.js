const appRoot = require('app-root-path');
const winston = require('winston');

const options = {
    file: {
        level: 'info',
        filename: `${appRoot}/logs/app.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
};

const customizedFormat = winston.format.combine(
    winston.format.colorize(),
    winston.format.timestamp(),
    winston.format.align(),
    winston.format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}`),
);

const transports = [new winston.transports.Console(options.console)];

if (process.env.NODE_ENV === 'production') {
    transports.push(new winston.transports.File(options.file));
}

const logger = new winston.createLogger({
    transports,
    exitOnError: false,
    format: customizedFormat,
});

logger.stream = {
    write: (message) => {
        logger.info(message);
    },
};

module.exports = logger;
